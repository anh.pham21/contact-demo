/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import ContactScreen from './screens/ContactScreen';

AppRegistry.registerComponent(appName, () => () => <ContactScreen />);
