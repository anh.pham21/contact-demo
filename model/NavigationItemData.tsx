export type NavigationItemDataType = {
  id: number;
  content: string;
};
