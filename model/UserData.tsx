export type UserCharacterDataType = {
  fullName: string;
  phoneNumber: string;
};

export type ResponseDataType = {
  first_name: string;
  last_name: string;
  phoneNumber: string;
};

export type CharacterListItemType = {
  character: string;
  data: UserCharacterDataType[];
};

export type CharacterListArrayType = CharacterListItemType[];
