export const Icon = {
  searchIcon:
    'https://img.mservice.io/momo_app_v2/new_version/img/appx_icon/16_navigation_search.png',
  pinIcon:
    'https://uxwing.com/wp-content/themes/uxwing/download/tools-equipment-construction/pin-icon.png',
  heartIconOn:
    'https://img.mservice.io/momo_app_v2/new_version/img/appx_icon/16_reaction_heart_full.png',
  heartIconOff:
    'https://img.mservice.io/momo_app_v2/new_version/img/appx_icon/16_reaction_heart.png',
  errorSearchIcon:
    'https://img.mservice.io/momo_app_v2/new_version/img/appx_icon/24_navigation_close_circle_full.png',
};
