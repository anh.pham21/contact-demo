import AsyncStorage from '@react-native-async-storage/async-storage';
// Import Type Section
import {
  UserCharacterDataType,
  CharacterListItemType,
} from '../../model/UserData';
// Set Tyoe of Props
interface CharacterObjectItem {
  [key: string]: {
    data: {
      userData: UserCharacterDataType;
    }[];
    title: string;
  };
}
// Store Data From API To Cache Function
export const storeDataFromApiToCache = async (key: string, value: string) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.error('Error saving data', error);
  }
};
// Group Object To Array Contact Function
export const groupObjectToArrayContact = (rawList: UserCharacterDataType[]) => {
  const objectContact = groupContact(rawList);
  const resultOfContact = convertObjectToArrayContact(objectContact);
  return resultOfContact;
};
const groupContact = (rawList: UserCharacterDataType[]) => {
  const groupContactObject = rawList.reduce(
    (preValue: CharacterObjectItem, user: UserCharacterDataType) => {
      const firstChar = user.fullName.charAt(0);
      if (!preValue.hasOwnProperty(firstChar)) {
        return {
          ...preValue,
          [firstChar]: {
            title: firstChar,
            data: [{userData: user}],
          },
        };
      } else {
        preValue[firstChar].data = [
          ...preValue[firstChar].data,
          {userData: user},
        ];
        return preValue;
      }
    },
    {},
  );
  return groupContactObject;
};
// Convert Object To Array Contact Function
const convertObjectToArrayContact = (
  groupObjectContact: CharacterObjectItem,
) => {
  const characterListArray = Object.entries(groupObjectContact).map(
    ([character, data]) => ({
      character,
      data: data.data.map(item => ({
        fullName: item.userData.fullName,
        phoneNumber: item.userData.phoneNumber,
      })),
    }),
  );
  return characterListArray;
};
// Sort Contact List Function
export const sortContactList = (contactList: CharacterListItemType[]) => {
  const sortedContactList = contactList.sort((a, b) => {
    if (
      a.character
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D') <
      b.character
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D')
    ) {
      return -1;
    }
    if (
      a.character
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D') >
      b.character
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D')
    ) {
      return 1;
    }
    return 0;
  });
  return sortedContactList;
};
// Filter Contact List
export const filterContactList = (
  searchData: UserCharacterDataType[],
  inputValue: string,
) => {
  const filteredData = searchData.filter(item => {
    const {fullName, phoneNumber} = item || {};
    return (
      fullName.includes(inputValue) ||
      fullName.toLowerCase().includes(inputValue) ||
      fullName.toUpperCase().includes(inputValue) ||
      fullName
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D')
        .includes(inputValue) ||
      fullName
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D')
        .toLowerCase()
        .includes(inputValue) ||
      fullName
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '')
        .replace(/đ/g, 'd')
        .replace(/Đ/g, 'D')
        .toUpperCase()
        .includes(inputValue) ||
      phoneNumber.includes(inputValue)
    );
  });
  return filteredData;
};

// Check Favorite User Item in List Function
export const isItemInFavoriteList = (
  list: UserCharacterDataType[],
  userItem: UserCharacterDataType,
) => {
  return list.some(
    favoriteItem => favoriteItem.phoneNumber === userItem.phoneNumber,
  );
};
