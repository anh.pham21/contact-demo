import {groupObjectToArrayContact} from './contact';

// Test and Expect Result
const successResult = [
  {
    character: 'P',
    data: [
      {
        fullName: 'Phạm Đức Anh',
        phoneNumber: '0123456789',
      },
      {
        fullName: 'Phạm Đức Anh 1',
        phoneNumber: '0123456789',
      },
      {
        fullName: 'Phạm Đức Anh 2',
        phoneNumber: '0123456789',
      },
    ],
  },
  {
    character: 'N',
    data: [
      {
        fullName: 'Nguyễn Đức Anh',
        phoneNumber: '0123456789',
      },
      {
        fullName: 'Nguyễn Đức Anh 1',
        phoneNumber: '0123456789',
      },
    ],
  },
  {
    character: 'T',
    data: [
      {
        fullName: 'T Đức Anh',
        phoneNumber: '0123456789',
      },
    ],
  },
];

const arrayBeforeProcess = [
  {
    fullName: 'Phạm Đức Anh',
    phoneNumber: '0123456789',
  },
  {
    fullName: 'Phạm Đức Anh 1',
    phoneNumber: '0123456789',
  },
  {
    fullName: 'Phạm Đức Anh 2',
    phoneNumber: '0123456789',
  },
  {
    fullName: 'Nguyễn Đức Anh',
    phoneNumber: '0123456789',
  },
  {
    fullName: 'Nguyễn Đức Anh 1',
    phoneNumber: '0123456789',
  },
  {
    fullName: 'T Đức Anh',
    phoneNumber: '0123456789',
  },
];

// Test Process to get Character List function
describe('groupObjectToArrayContact()', () => {
  it('should success when array has sorted', () => {
    expect(groupObjectToArrayContact(arrayBeforeProcess)).toEqual(successResult);
  });
  it('when array is []', () => {
    expect(groupObjectToArrayContact([])).toHaveLength(0);
  });
});
