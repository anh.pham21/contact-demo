import React from 'react';
import {StyleSheet, View} from 'react-native';
// Import Component Section
import ContactFavoriteUserItem from './ContactFavoriteUserItem';
// Import Type Section
import {UserCharacterDataType} from '../../model/UserData';
// Set Type of Props
interface ContactFavoriteUserItemProps {
  favoriteList: UserCharacterDataType[];
}

// Favorite User List Component
const ContactFavoriteUserList = (props: ContactFavoriteUserItemProps) => {
  const {favoriteList} = props || {};
  return (
    <View style={styles.favoriteListContainer}>
      {favoriteList.map((item, index) => (
        <ContactFavoriteUserItem key={index} item={item} />
      ))}
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  favoriteListContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

export default ContactFavoriteUserList;
