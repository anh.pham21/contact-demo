import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
// Import Type Section
import {CharacterListItemType} from '../../model/UserData';
// Set Type of Props
interface UserItemHeaderProps {
  section: CharacterListItemType;
}
// Header Character of User Item Component
const ContactUserItemHeader = (props: UserItemHeaderProps) => {
  const {section} = props || {};
  const {character} = section || {};
  return (
    <View style={styles.characterContainer}>
      <Text style={styles.characterText}>{character}</Text>
    </View>
  );
};
// Style Section
const styles = StyleSheet.create({
  characterContainer: {
    paddingVertical: 8,
    paddingHorizontal: 12,
    gap: 4,
    backgroundColor: '#F9F9F9',
  },
  characterText: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '600',
  },
});

export default ContactUserItemHeader;
