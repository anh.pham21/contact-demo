import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {Icon} from '../assets/icon/icon';

// Exception Search Case Component
const ContactUserEmptyItem = () => {
  return (
    <View style={styles.emptyItemContainer}>
      <Image
        style={styles.emptyItemImage}
        source={{
          uri: Icon.errorSearchIcon,
        }}
      />
      <Text style={styles.emptyItemText}>
        Can't find. Please search again !!!
      </Text>
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  emptyItemContainer: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 80,
    borderRadius: 20,
  },
  emptyItemImage: {
    resizeMode: 'contain',
    width: 100,
    height: 100,
    tintColor: '#000',
    marginBottom: 20,
  },
  emptyItemText: {
    marginBottom: 60,
    fontWeight: '600',
    fontStyle: 'italic',
  },
});

export default ContactUserEmptyItem;
