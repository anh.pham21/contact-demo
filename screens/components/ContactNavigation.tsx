import React from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
// Import Component Section
import ContactNavigationItem from './ContactNavigationItem';
// Import Type Section
import {NavigationItemDataType} from '../../model/NavigationItemData';

// Example Array Data of Navigation Section
const itemList = [
  {
    id: 1,
    content: 'Tất cả',
  },
  {
    id: 2,
    content: 'Danh bạ máy',
  },
  {
    id: 3,
    content: 'Danh bạ MoMo',
  },
  {
    id: 4,
    content: 'Bạn bè MoMo',
  },
];

// Navigation Item Component
const NavigationItem = ({item}: {item: NavigationItemDataType}) => {
  return <ContactNavigationItem item={item} />;
};

// Navigation Component
const ContactNavigation = () => {
  return (
    <View style={styles.navigationContainer}>
      <FlatList horizontal={true} data={itemList} renderItem={NavigationItem} />
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  navigationContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 12,
    paddingHorizontal: 4,
  },
});

export default ContactNavigation;
