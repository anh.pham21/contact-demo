import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
// Import Type Section
import {UserCharacterDataType} from '../../model/UserData';
// Import Assets Section
import {Icon} from '../assets/icon/icon';
import {ImageLink} from '../assets/image/image';

// Favorite User Item Component
const ContactFavoriteUserItem = ({item}: {item: UserCharacterDataType}) => {
  const {fullName} = item || {};
  return (
    <View style={styles.favoriteItemContainer}>
      {/* Favorite User Avatar Section */}
      <View style={styles.favoriteAvatarContain}>
        <Image source={ImageLink.userAva} style={styles.favoriteAvatar} />
        <TouchableOpacity style={styles.favoriteCloseButton}>
          <Image
            source={{
              uri: Icon.pinIcon,
            }}
            style={styles.favoriteCloseImg}
          />
        </TouchableOpacity>
      </View>
      {/* Favorite User Name Section */}
      <View style={styles.favoriteNameContainer}>
        <Text style={styles.favoriteName}>{fullName}</Text>
      </View>
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  favoriteItemContainer: {
    flexDirection: 'column',
    marginLeft: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 12,
    width: 70,
  },
  favoriteAvatarContain: {
    position: 'relative',
  },
  favoriteAvatar: {
    width: 50,
    height: 50,
    resizeMode: 'stretch',
    borderRadius: 100,
  },
  favoriteCloseButton: {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: '#fff',
    borderRadius: 100,
  },
  favoriteCloseImg: {
    width: 16,
    height: 16,
    borderRadius: 100,
    resizeMode: 'stretch',
    tintColor: '#484848',
  },
  favoriteNameContainer: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  favoriteName: {
    color: '#000',
    fontSize: 12,
  },
});

export default ContactFavoriteUserItem;
