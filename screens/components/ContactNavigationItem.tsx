import React, {useState} from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

// Import Type Section
import {NavigationItemDataType} from '../../model/NavigationItemData';

// Set Type of Props
interface ContactNavigationItemProps {
  item: NavigationItemDataType;
}

// Navigation Item Component
const ContactNavigationItem = (props: ContactNavigationItemProps) => {
  const {item} = props || {};
  const [pressStatus, setPressStatus] = useState<boolean>(true);
  // Handle Press Function
  const handlePress = () => {
    setPressStatus(!pressStatus);
  };
  return (
    <TouchableOpacity
      style={pressStatus ? styles.navigationItem : styles.navigationItemOnPress}
      onPress={handlePress}>
      <Text
        style={
          pressStatus
            ? styles.navigationItemText
            : styles.navigationItemTextOnPress
        }>
        {item.content}
      </Text>
    </TouchableOpacity>
  );
};

// Style Section
const styles = StyleSheet.create({
  navigationItem: {
    borderRadius: 16,
    backgroundColor: '#fff',
    paddingHorizontal: 6,
    paddingVertical: 8,
    gap: 4,
    marginLeft: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  navigationItemOnPress: {
    borderWidth: 2,
    borderColor: '#F7ACD5',
    borderRadius: 16,
    paddingHorizontal: 3.5,
    paddingVertical: 6,
    gap: 4,
    marginLeft: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  navigationItemText: {
    color: '#484848',
    fontWeight: '400',
    fontSize: 14,
    lineHeight: 20,
  },
  navigationItemTextOnPress: {
    color: '#EB2F96',
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 20,
  },
});

export default ContactNavigationItem;
