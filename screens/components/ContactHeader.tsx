import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  TextInput,
} from 'react-native';

// Set Type of Props
interface ContactHeaderProps {
  setInputValue: (text: string) => void;
}
// Import Assets Section
import {Icon} from '../assets/icon/icon';

// Header Component
const ContactHeader = (props: ContactHeaderProps) => {
  const {setInputValue} = props || {};
  const getTextInput = (text: string) => {
    setInputValue(text);
  };
  const cancelInputValue = () => {
    setInputValue('');
  };
  return (
    <View style={styles.headerContainer}>
      {/* Search Box Section */}
      <View style={styles.searchBox}>
        {/* Search Icon */}
        <Image
          style={styles.searchIcon}
          source={{
            uri: Icon.searchIcon,
          }}
        />
        {/* Search Text Input */}
        <TextInput
          placeholder="Tìm tên, số điện thoại"
          autoCapitalize="none"
          onChangeText={getTextInput}
          autoCorrect={false}
        />
      </View>
      {/* Cancel Button Section */}
      <TouchableOpacity
        style={styles.headerCancelBtn}
        onPress={cancelInputValue}>
        <Text style={styles.headerCancelBtnText}>Huỷ</Text>
      </TouchableOpacity>
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 12,
  },
  searchBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderRadius: 40,
    paddingHorizontal: 8,
    paddingVertical: 6,
    backgroundColor: '#fff',
    width: '80%',
  },
  searchIcon: {
    width: 24,
    height: 24,
    resizeMode: 'contain',
    tintColor: '#727272',
    paddingRight: 8,
  },
  headerInput: {
    fontWeight: '400',
    fontSize: 15,
  },
  headerCancelBtn: {
    width: 33,
  },
  headerCancelBtnText: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 22,
  },
});

export default ContactHeader;
