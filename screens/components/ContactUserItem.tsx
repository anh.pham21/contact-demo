import React from 'react';
import {View, Image, Text, StyleSheet, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
// Import Type Section
import {UserCharacterDataType} from '../../model/UserData';
// Import Function Section
import {isItemInFavoriteList} from '../utils/contact';
// Set Type of Props
interface UserItemProps {
  item: UserCharacterDataType;
  favoriteList: UserCharacterDataType[];
  setFavoriteList: (user: UserCharacterDataType[]) => void;
}
// Import Assets Section
import {Icon} from '../assets/icon/icon';
import {ImageLink} from '../assets/image/image';

// Contact User Item Component
const ContactUserItem = (props: UserItemProps) => {
  const {item, favoriteList, setFavoriteList} = props || {};
  const {fullName, phoneNumber} = item || {};
  // Push User Item in Favorite List Function
  const pushFavoriteList = () => {
    const favoritesWithNewItem = [...favoriteList, item];
    AsyncStorage.setItem(
      'favoriteUserCache',
      JSON.stringify(favoritesWithNewItem),
    );
    setFavoriteList(favoritesWithNewItem);
  };
  // Delete User Item in Favorite List Function
  const delFavoriteList = () => {
    const favoriteListAfterDeleted = favoriteList.filter(userItem => {
      if (userItem.phoneNumber !== phoneNumber) {
        return item;
      }
    });
    AsyncStorage.setItem(
      'favoriteUserCache',
      JSON.stringify(favoriteListAfterDeleted),
    );
    setFavoriteList(favoriteListAfterDeleted);
  };
  // Handle Press Heart Icon Function
  const onPressHeartIcon = () => {
    if (isItemInFavoriteList(favoriteList, item)) {
      delFavoriteList();
    } else {
      pushFavoriteList();
    }
  };
  return (
    <View style={styles.userItemContain}>
      {/* User Item Avatar Section */}
      <View>
        <Image source={ImageLink.userAva} style={styles.userAvatar} />
        <Image
          source={{
            uri: ImageLink.subAvatar,
          }}
          style={styles.userSubAvatar}
        />
      </View>
      {/* User Item Information Section */}
      <View style={styles.userInfo}>
        <View>
          <Text style={styles.userName}>{`${fullName}`}</Text>
          <Text style={styles.userPhone}>{phoneNumber}</Text>
        </View>
        {/* User Item Information Favorite Button Section */}
        <TouchableOpacity onPress={onPressHeartIcon}>
          <Image
            style={
              isItemInFavoriteList(favoriteList, item) === true
                ? styles.loveBtnOn
                : styles.loveBtnOff
            }
            source={{
              uri:
                isItemInFavoriteList(favoriteList, item) === true
                  ? Icon.heartIconOn
                  : Icon.heartIconOff,
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

// Style Section
const styles = StyleSheet.create({
  userItemContain: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: '#fff',
    paddingHorizontal: 12,
    paddingVertical: 6,
    borderRadius: 12,
  },
  userAvatar: {
    width: 40,
    height: 40,
    resizeMode: 'stretch',
    borderRadius: 100,
    position: 'relative',
  },
  userSubAvatar: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    resizeMode: 'contain',
    width: 11,
    height: 11,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#fff',
  },
  userInfo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 8,
  },
  userName: {
    fontSize: 12,
    lineHeight: 12,
    fontWeight: '600',
  },
  userPhone: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: '400',
    color: '#727272',
  },
  loveBtnOn: {
    width: 24,
    height: 24,
    tintColor: '#cf1322',
  },
  loveBtnOff: {
    width: 24,
    height: 24,
    tintColor: '#484848',
  },
});

export default ContactUserItem;
