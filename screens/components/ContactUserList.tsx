import React, {useEffect, useState} from 'react';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {SectionList, View, ActivityIndicator, StyleSheet} from 'react-native';

// Import Type Section
import {
  CharacterListItemType,
  UserCharacterDataType,
  ResponseDataType,
} from '../../model/UserData';

// Import Component Section
import ContactUserItem from './ContactUserItem';
import ContactUserItemHeader from './ContactUserItemHeader';
import ContactUserEmptyItem from './ContactUserEmptyItem';

// Import Function Section
import {
  groupObjectToArrayContact,
  sortContactList,
  filterContactList,
  storeDataFromApiToCache,
} from '../utils/contact';

// Set Type of Props
interface ContactUserListProps {
  inputValue: string;
  favoriteList: UserCharacterDataType[];
  setFavoriteList: (user: UserCharacterDataType[]) => void;
}

// Contact User List Component
const ContactUserList = (props: ContactUserListProps) => {
  const {inputValue, favoriteList, setFavoriteList} = props || {};
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [userData, setUserData] = useState<CharacterListItemType[]>([]);
  const [searchData, setSearchData] = useState<UserCharacterDataType[]>([]);
  // Get Data from API
  const getDataFromApi = () => {
    axios
      .get('https://www.mockachino.com/17acefab-0956-47/contacts')
      .then(res => {
        const processedData: UserCharacterDataType[] =
          res.data.contacts.map((data: ResponseDataType) => ({
            fullName: `${data.last_name} ${data.first_name}`,
            phoneNumber: `${data.phoneNumber}`,
          })) || [];
        const contactList: CharacterListItemType[] =
          groupObjectToArrayContact(processedData) || [];
        setSearchData(processedData);
        const sortedContactList = sortContactList(contactList) || [];
        setUserData(sortedContactList);
        setIsLoading(false);
        storeDataFromApiToCache('cacheData', JSON.stringify(processedData));
      });
  };
  // Get Data from Cache
  const getDataFromCache = () => {
    AsyncStorage.getItem('cacheData').then(cache => {
      if (typeof cache === 'string') {
        const cacheDataStored = JSON.parse(cache) || {};
        const contactList: CharacterListItemType[] =
          groupObjectToArrayContact(cacheDataStored) || [];
        setSearchData(cacheDataStored);
        const sortedContactList = sortContactList(contactList) || [];
        setUserData(sortedContactList);
      }
      setIsLoading(false);
    });
  };

  // Load Data From Cache or Api
  useEffect(() => {
    setIsLoading(true);
    getDataFromCache();
    getDataFromApi();
  }, []);

  // Main Search Function
  useEffect(() => {
    const filteredData = filterContactList(searchData, inputValue) || [];
    const filteredDataContact = groupObjectToArrayContact(filteredData) || [];
    const sortedFilteredContactList =
      sortContactList(filteredDataContact) || [];
    setUserData(sortedFilteredContactList);
  }, [inputValue, searchData]);

  // Header Character of User Item Component
  const userItemHeader = ({section}: {section: CharacterListItemType}) => {
    return <ContactUserItemHeader section={section} />;
  };

  // Contact User Item List Component
  const userItem = ({item}: {item: UserCharacterDataType}) => {
    return (
      <ContactUserItem
        item={item}
        favoriteList={favoriteList}
        setFavoriteList={setFavoriteList}
      />
    );
  };

  // Exception Search Case Component
  const userEmptyItem = () => {
    return <ContactUserEmptyItem />;
  };
  // Loading Event
  if (isLoading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator size={'large'} color={'#484848'} />
      </View>
    );
  }
  return (
    <SectionList
      sections={userData}
      renderItem={userItem}
      renderSectionHeader={userItemHeader}
      keyExtractor={(item, index) => index.toString()}
      ListEmptyComponent={userEmptyItem}
    />
  );
};

// Style Section
const styles = StyleSheet.create({
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 80,
    borderRadius: 20,
    marginBottom: 20,
  },
});

export default ContactUserList;
