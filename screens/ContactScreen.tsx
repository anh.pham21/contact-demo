import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, StatusBar, Text, View} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
// Import Component Section
import ContactHeader from './components/ContactHeader';
import ContactNavigation from './components/ContactNavigation';
import ContactUserList from './components/ContactUserList';
import ContactFavoriteUserList from './components/ContactFavoriteUserList';
// Import Type Section
import {UserCharacterDataType} from '../model/UserData';

// Main Component Section
const ContactScreen = () => {
  // The inputValue state is the value of the Search Box as the user entered
  const [inputValue, setInputValue] = useState<string>('');
  // The favoriteList state is the array value of the Favorite User Array as the user selected
  const [favoriteList, setFavoriteList] = useState<UserCharacterDataType[]>([]);
  // Get Favorite User List From Cache
  useEffect(() => {
    AsyncStorage.getItem('favoriteUserCache').then(cache => {
      if (cache !== null) {
        const favoriteUserCache = JSON.parse(cache);
        setFavoriteList(favoriteUserCache);
      } else {
        setFavoriteList([]);
      }
    });
  }, []);

  return (
    <SafeAreaView style={styles.fullScreen}>
      <View style={styles.pinkBackground} />
      <StatusBar barStyle={'light-content'} />
      {/* Header Section */}
      <ContactHeader setInputValue={setInputValue} />
      {/* Navigation Section */}
      <ContactNavigation />
      {/* Content Section */}
      <View style={styles.body}>
        {/* Favorite User List Section */}
        <View
          style={
            favoriteList.length !== 0
              ? styles.userFavoriteContainer
              : styles.hiddenFavoriteContainer
          }>
          <Text style={styles.userListTitle}>Đề xuất</Text>
          <ContactFavoriteUserList favoriteList={favoriteList} />
        </View>
        {/* Contact User List Section */}
        <View style={styles.userListContainer}>
          <Text style={styles.userListTitle}>Danh bạ máy</Text>
          <ContactUserList
            inputValue={inputValue}
            favoriteList={favoriteList}
            setFavoriteList={setFavoriteList}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

// Style Section
const styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: '#F2F2F6',
    position: 'relative',
  },
  pinkBackground: {
    position: 'absolute',
    top: 0,
    backgroundColor: '#EB2F96',
    width: '100%',
    height: 100,
  },
  body: {
    paddingHorizontal: 12,
  },
  hiddenFavoriteContainer: {
    display: 'none',
  },
  userFavoriteContainer: {
    borderRadius: 12,
    gap: 12,
    backgroundColor: '#fff',
    marginBottom: 15,
  },
  userListContainer: {
    borderRadius: 12,
    gap: 12,
    backgroundColor: '#fff',
    marginBottom: 500,
  },
  userListTitle: {
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 20,
    padding: 12,
  },
});

export default ContactScreen;
